import React from 'react';
import { View, FlatList, Dimensions } from 'react-native';
import styles from '../cardslist/style';

import cars from '../cardslist/card'
import CardItem from '../cardItem';

const CardsList = (props) => {
    return(
        <View style={styles.container}>
            <FlatList
                data={cars}
                renderItem= {({item})=> <CardItem car={item}/>}
                showsVerticalScrollIndicator={false}
                snapToAlignment={'start'}
                decelerationRate={'fast'}
                snapToInterval={Dimensions.get('window').height}
            />
        </View>
    )
}


export default CardsList;