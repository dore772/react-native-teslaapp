import React from "react";
import { View, Text, Pressable } from "react-native";
import styles from "../../components/styledbutton/style";

const StyledButton = (props) => {

    /* const type = props.type;
    const content = props.content;
    const onPress = props.onPress; */

    const { content, onPress, type } = props;

    const backgroundColor = type === 'primary' ? '#171A20CC' : '#FFFFFFA5';
    const textColor = type === 'primary' ? '#FFFFFF' : '#171A20';

    return(
        <View style={styles.container}>
            <Pressable 
                style={[styles.button, {backgroundColor: backgroundColor}]}
                onPress={() => onPress()}
            >
                <Text style={[styles.text, {color: textColor}]}>{content}</Text>
            </Pressable>
        </View>
    )
}

export default StyledButton;