import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
     container: {
        width:'100%',
        padding: 10
      },
      button: {
        backgroundColor: 'yellow',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        height: 40
      },
      text: {
        fontSize:12,
        fontWeight:'500',
        textTransform: 'uppercase'
      }
});

export default styles;