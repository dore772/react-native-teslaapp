import React from 'react';
import { Text, View, ImageBackground } from 'react-native';
import styles from './styles';

import StyledButton from '../../components/styledbutton'

const CardItem = (props) => {

    const { name, tagline, taglineCTA, image } = props.car;

    return (
        <View style={styles.carContainer}>
            <ImageBackground source={image} style={styles.image}/>
            <View style={styles.titles}>
                <Text style={styles.title}>{name}</Text>
                <Text style={styles.subtitle}>
                    {tagline}
                    {" "}
                    <Text style={styles.subtitleCTA}>{taglineCTA}</Text>
                </Text>
            </View>
            <View style={styles.buttonsContainer}>
                <StyledButton 
                    type="primary" 
                    content={"Commande personnalisée"}
                    onPress= {()=>{
                        console.log("Commande personnalisée a été cliqué")
                    }}
                />
                <StyledButton 
                    type="secondary" 
                    content={"Inventaire actuel"}
                    onPress= {()=>{
                        console.log("Inventaire actuel a été cliqué")
                    }}
                />
            </View>
            
        </View>
    )
}

export default CardItem;